import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex);
export default new Vuex.Store({
    state: {
        userInfo:null,
        currency:null,
    },
    getters:{
        userInfo(state){
            if(!state.userInfo){
                return state.userInfo = JSON.parse(sessionStorage.getItem('userInfo'));
            }
            return state.userInfo;
        },
        isLogin(state){
            if(!state.isLogin) {
                state.isLogin=sessionStorage.getItem('isLogin');   //从sessionStorage中读取状态
                state.username=sessionStorage.getItem('username');
            }
            return state.isLogin
        }
    },
    mutations: {
        getCurrentUserInfo(state,data){
            state.userInfo=data;
        },
        delAllUserInfo(){
            sessionStorage.removeItem('userInfo');
            sessionStorage.removeItem('token')
        },
        displayCorrespondingData(state,data){
            state.currency=data;
        }
    },
    actions: {
        getCurrentUserInfo(state,data){
            state.commit('getCurrentUserInfo',data);
        },
        delAllUserInfo(state){
            state.commit('delAllUserInfo')
        },
        displayCorrespondingData(state,data){
            state.commit('displayCorrespondingData',data);
        }
    },
  })
