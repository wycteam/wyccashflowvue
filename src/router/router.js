import Vue from 'vue';
import Router from 'vue-router';
import Notfind from '../views/404.vue';
import Index from '../components/layout/index.vue';
import Login from "../views/Login.vue";
import Infoshow from "../components/userinfo/components/Infoshow.vue";
import Foundlist from "../components/capitalflow/components/Foundlist.vue";
import Servicefeeflow from "../components/capitalflow/components/Servicefeeflow.vue";
import Usermanagement from "../components/userinfo/components/Usermanagement.vue"

Vue.use(Router);
const router=new Router({
    //mode: 'history',
    //base: process.env.BASE_URL,
    routes:[
        {path:'*',name:'/404',component:Notfind},
        {path:'/',redirect:'/index'},
        {path:'/login',name:'login',component:Login},
        {path:'/index',
            //name:'index',当某个路由有子集路由的时候，这时候父级路由需要一个默认的路由，所以父级路由不能定义name属性。
            component:Index,
            children:[{
                path:'',
                component:Foundlist
            },{
                path:'/Foundlist',
                name:'Foundlist',
                component:Foundlist
            },{
                path:'/servicefeeflow',
                name:'servicefeeflow',
                //meta:{role: ['admin']},
                component:Servicefeeflow
            },{
                path:'/infoshow',
                name:'infoshow',
                component:Infoshow
            },{
                path:'/usermanagement',
                name:'usermanagement',
                component:Usermanagement
            }]},
    ]
  });
//路由守卫
router.beforeEach((to,from,next)=>{
    const isLogin=sessionStorage.token?true:false;
    const authority=JSON.parse(sessionStorage.getItem('userInfo'));
    if(to.path=="/login"||to.path=='/register'){
        next();
    }else{
        isLogin?next():next('/login');
    }
    if(authority){
        if(authority.identity=='admin'&&to.path=='/usermanagement'){
            next('/Foundlist');
        }else if(authority.identity=='user'&&to.path=='/servicefeeflow'||authority.identity=='user'&&to.path=='/usermanagement'){
            next('/Foundlist');
        }else if(authority.defaulttag=='1'){
            next('/infoshow')
        }
    }
});
export default router;
