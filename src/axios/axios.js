import Axios from 'axios';
import { Message, Loading } from 'element-ui';
import Router from "../router/router.js";
let loading;      //定义loading变量
function startLoading(){    //使用Element loading-start 方法
    loading = Loading.service({
        lock:true,
        text:'Loading...',
        background:'rgba(0, 0, 0, 0.7)'
    })
}
function endLoading(){    //使用Element loading-close 方法
    loading.close();
}
// 请求拦截
Axios.interceptors.request.use(config => {

    // 加载
    startLoading();
    //如果这个token存在
    if (sessionStorage.token){
        //设置统一header
        config.headers.Authorization = sessionStorage.token;
    }
        return config
}, error => {
    return Promise.reject(error)
});
// 响应拦截  401 token过期处理
Axios.interceptors.response.use(response => {
    endLoading();
    return response
}, error=>{
    //错误提醒
    endLoading();
    //Message.error(error.response.data);
    //获取错误状态码
    const { status }=error.response;
    if(status == 401){
        Message.error('为保护帐号安全，请重新登录');
        // 清除token和信息
        sessionStorage.removeItem('token');
        sessionStorage.removeItem('userInfo');
        // 页面跳转
        Router.push('/login');
    }
    return Promise.reject(error)
});
export default Axios