import Vue from 'vue'
import App from './App.vue'
import router from './router/router.js'
import store from './store/store.js'
import Axios from "./axios/axios.js"
import './plugins/element.js'
import i18n from './i18n/i18n.js';
Vue.config.productionTip = false;
Vue.prototype.$axios=Axios;
new Vue({
  router,
  store,
  Axios,
  i18n,
  components: {App} ,//这个写法是export default导出变量,通过import导入APP变量
  render: h => h(App)
}).$mount('#app');
